package
{
	import flash.desktop.NativeApplication;
	import flash.display.Sprite;
	import flash.display3D.Context3DProfile;
	import flash.display3D.Context3DRenderMode;
	import flash.events.Event;
	import flash.utils.setTimeout;
	
	import feathers.utils.ScreenDensityScaleFactorManager;
	
	import ik.code.sudokouts.Main;
	
	import starling.core.Starling;
	
	[SWF(width="640",height="960",frameRate="60",backgroundColor="#000000")]
	public class SudoKouts extends Sprite
	{
		private var starling:Starling;
		public function SudoKouts()
		{
			super();
			loaderInfo.addEventListener(Event.COMPLETE, initialize);
		}
		
		private function initialize(e:Event):void
		{
			loaderInfo.removeEventListener(Event.COMPLETE, initialize);
			setTimeout(function():void
			{
				starling = new Starling(Main, stage, null, null, Context3DRenderMode.AUTO, Context3DProfile.BASELINE);
				
				var scaler:ScreenDensityScaleFactorManager = new ScreenDensityScaleFactorManager(starling);
				starling.supportHighResolutions = true;
				stage.addEventListener(Event.DEACTIVATE, stageDeactivated);
				stage.addEventListener(Event.ACTIVATE, stageActivated);
				starling.start();
				
				NativeApplication.nativeApplication.addEventListener(Event.DEACTIVATE, exit);
			}, 500);
		}
		
		protected function exit(event:Event):void
		{
			NativeApplication.nativeApplication.exit();
		}
		
		private function stageDeactivated(e:Event):void
		{
			starling.stop(true);
		}
		
		private function stageActivated(e:Event):void
		{
			starling.start();
		}
	}
}