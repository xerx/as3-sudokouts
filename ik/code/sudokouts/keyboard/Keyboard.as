package ik.code.sudokouts.keyboard
{
	
	import starling.display.DisplayObjectContainer;
	import starling.events.Event;
	
	public class Keyboard extends DisplayObjectContainer
	{
		private var padView:PadView;
		private var numericView:NumericView;
		
		public function Keyboard()
		{
			super();
			
			padView = new PadView(this);
			numericView = new NumericView(this);
			
			addChild(padView);
			addChild(numericView);
			
			padView.addEventListener("buttonClicked", buttonClicked);
			numericView.addEventListener("buttonClicked", buttonClicked);
			setView(PadView.ID);
		}
		
		private function buttonClicked(e:Event):void
		{
			if(e.data == KeyboardButton.BACK)
				setView(PadView.ID);
		}
		public function setView(value:int):void
		{
			padView.visible = value == PadView.ID;
			numericView.visible = value == NumericView.ID;
		}
		public function get view():KeyboardView
		{
			return padView.visible ? padView:numericView; 
		}
		public function get pencilIsActive():Boolean
		{
			return view.pencilIsDown;
		}
		override public function dispose():void
		{
			padView = null;
			numericView = null;
			
			super.dispose();
		}
	}
}