package ik.code.sudokouts.keyboard
{
	import feathers.controls.Button;
	
	public class KeyboardButton extends Button
	{
		public static const UP:String = "up";
		public static const DOWN:String = "down";
		public static const LEFT:String = "left";
		public static const RIGHT:String = "right";
		public static const SELECT:String = "select";
		
		public static const PENCIL:String = "pencil";
		public static const HINT:String = "hint";
		public static const BACK:String = "back";
		
		public static const ONE:String = "1";
		public static const TWO:String = "2";
		public static const THREE:String = "3";
		public static const FOUR:String = "4";
		public static const FIVE:String = "5";
		public static const SIX:String = "6";
		public static const SEVEN:String = "7";
		public static const EIGHT:String = "8";
		public static const NINE:String = "9";
		
		public var id:String;
		
		
		public function KeyboardButton()
		{
			super();
		}
		
		
	}
}