package ik.code.sudokouts.keyboard
{
	import feathers.controls.ButtonState;
	import feathers.controls.ScrollContainer;
	import feathers.controls.ToggleButton;
	import feathers.layout.AnchorLayout;
	
	import ik.code.sudokouts.EmbeddedAssets;
	import ik.code.sudokouts.Globals;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public class KeyboardView extends ScrollContainer
	{
		protected static const GAP:int = 1;
		protected static var pencilIsSelected:Boolean;
		
		protected var buttonsWidth:int;
		protected var buttonsHeight:int;
		protected var extraButtonsGap:int;
	
		protected var pencilButton:ToggleButton;
		protected var keyboard:Keyboard;
		
		public function KeyboardView(keyboard:Keyboard)
		{
			super();
			
			pencilIsSelected = false;
			this.keyboard = keyboard;
			
			horizontalScrollPolicy = verticalScrollPolicy = "off";
			
			layout = new AnchorLayout();
			
			buttonsWidth = Math.floor((Starling.current.stage.stageWidth - GAP * 3) * .25);
			buttonsHeight = Math.ceil(Starling.current.stage.stageHeight * .09);
			extraButtonsGap = Starling.current.stage.stageWidth - buttonsWidth * 4 - GAP * 2;
			
			pencilButton = new ToggleButton();
			pencilButton.defaultIcon = new Image(EmbeddedAssets.PENCIL_ICON_TEXTURE);
			pencilButton.defaultSkin = new Quad(1, 1, 0x69747C);
			var down:Quad = new Quad(1, 1, 0xC03221);
			pencilButton.downSkin = down;
			pencilButton.setSkinForState(ButtonState.DOWN_AND_SELECTED, down);
			pencilButton.setSkinForState(ButtonState.UP_AND_SELECTED, down);
			pencilButton.setSkinForState(ButtonState.HOVER_AND_SELECTED, down);
			pencilButton.width = buttonsWidth;
			pencilButton.height = buttonsHeight * 2 + GAP;
			pencilButton.addEventListener(Event.TRIGGERED, buttonClicked);
			addChild(pencilButton);
		}
		private function buttonClicked(e:Event):void
		{
			var button:Object = e.currentTarget;
			var id:String;
			if(button is KeyboardButton)
			{
				id = (button as KeyboardButton).id;
			}
			else
			{
				id = KeyboardButton.PENCIL;
				pencilIsSelected = !pencilButton.isSelected;
			}
			dispatchEventWith("buttonClicked", true, id);
		}
		protected function buttonsFactory(id:String, label:String = null, icon:Texture = null, enabled:Boolean = true, color:uint = 0):KeyboardButton
		{
			var button:KeyboardButton = new KeyboardButton();
			button.id = id;
			button.width = buttonsWidth;
			button.height = buttonsHeight;
			button.isEnabled = enabled;
			button.addEventListener(Event.TRIGGERED, buttonClicked);
			
			if(label)
			{
				button.label = label;
				button.fontStyles = Globals.theme.lightBoldFontStyles;
			}
			if(icon)
			{
				button.defaultIcon = new Image(icon);
			}
			if(color)
			{
				button.defaultSkin = new Quad(1, 1, color);
			}
			return button;
		}
		override public function set visible(value:Boolean):void
		{
			super.visible = value;
			pencilButton.isSelected = pencilIsSelected;
		}
		public function get pencilIsDown():Boolean
		{
			return pencilButton.isSelected;
		}
		public static function get size():Object
		{
			return {width:Starling.current.stage.stageWidth, 
					height:Math.ceil(Starling.current.stage.stageHeight * .09) * 3 + GAP * 2};
		}
		override public function dispose():void
		{
			pencilButton = null;
			keyboard = null;
			super.dispose();
		}
	}
}