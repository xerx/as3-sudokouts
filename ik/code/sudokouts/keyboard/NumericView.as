package ik.code.sudokouts.keyboard
{
	import feathers.controls.Button;
	import feathers.layout.AnchorLayoutData;
	
	import ik.code.sudokouts.EmbeddedAssets;

	public class NumericView extends KeyboardView
	{
		public static const ID:int = 2;
		
		private var buttonOne:Button;
		private var buttonTwo:Button;
		private var buttonThree:Button;
		private var buttonFour:Button;
		private var buttonFive:Button;
		private var buttonSix:Button;
		private var buttonSeven:Button;
		private var buttonEight:Button;
		private var buttonNine:Button;
		private var backButton:Button;
		
		public function NumericView(keyboard:Keyboard)
		{
			super(keyboard);
			
			buttonOne = buttonsFactory(KeyboardButton.ONE, "1");
			addChild(buttonOne);
			
			buttonTwo = buttonsFactory(KeyboardButton.TWO, "2");
			addChild(buttonTwo);
			
			buttonThree = buttonsFactory(KeyboardButton.THREE, "3");
			addChild(buttonThree);
			
			buttonFour = buttonsFactory(KeyboardButton.FOUR, "4");
			addChild(buttonFour);
			
			buttonFive = buttonsFactory(KeyboardButton.FIVE, "5");
			addChild(buttonFive);
			
			buttonSix = buttonsFactory(KeyboardButton.SIX, "6");
			addChild(buttonSix);
			
			buttonSeven = buttonsFactory(KeyboardButton.SEVEN, "7");
			addChild(buttonSeven);
			
			buttonEight = buttonsFactory(KeyboardButton.EIGHT, "8");
			addChild(buttonEight);
			
			buttonNine = buttonsFactory(KeyboardButton.NINE, "9");
			addChild(buttonNine);
			
			backButton = buttonsFactory(KeyboardButton.BACK, null, EmbeddedAssets.LEFT_ARROW_ICON_TEXTURE, true, 0x69747C);
			addChild(backButton);
			
			var oneLD:AnchorLayoutData = new AnchorLayoutData(NaN, NaN, NaN, extraButtonsGap);
			oneLD.leftAnchorDisplayObject = pencilButton;
			buttonOne.layoutData = oneLD;
			
			var twoLD:AnchorLayoutData = new AnchorLayoutData(NaN, NaN, NaN, GAP);
			twoLD.leftAnchorDisplayObject = buttonOne;
			buttonTwo.layoutData = twoLD;
			
			var threeLD:AnchorLayoutData = new AnchorLayoutData(NaN, NaN, NaN, GAP);
			threeLD.leftAnchorDisplayObject = buttonTwo;
			buttonThree.layoutData = threeLD;
			
			var fourLD:AnchorLayoutData = new AnchorLayoutData(GAP, NaN, NaN, extraButtonsGap);
			fourLD.topAnchorDisplayObject = buttonOne;
			fourLD.leftAnchorDisplayObject = pencilButton;
			buttonFour.layoutData = fourLD;
			
			var fiveLD:AnchorLayoutData = new AnchorLayoutData(GAP, NaN, NaN, GAP);
			fiveLD.leftAnchorDisplayObject = buttonFour;
			fiveLD.topAnchorDisplayObject = buttonTwo;
			buttonFive.layoutData = fiveLD;
			
			var sixLD:AnchorLayoutData = new AnchorLayoutData(GAP, NaN, NaN, GAP);
			sixLD.leftAnchorDisplayObject = buttonFive;
			sixLD.topAnchorDisplayObject = buttonThree;
			buttonSix.layoutData = sixLD;
			
			var backLD:AnchorLayoutData = new AnchorLayoutData(GAP, NaN, NaN, NaN);
			backLD.topAnchorDisplayObject = pencilButton;
			backButton.layoutData = backLD;
			
			var sevenLD:AnchorLayoutData = new AnchorLayoutData(GAP, NaN, NaN, extraButtonsGap);
			sevenLD.leftAnchorDisplayObject = backButton;
			sevenLD.topAnchorDisplayObject = buttonFour;
			buttonSeven.layoutData = sevenLD;
			
			var eightLD:AnchorLayoutData = new AnchorLayoutData(GAP, NaN, NaN, GAP);
			eightLD.leftAnchorDisplayObject = buttonSeven;
			eightLD.topAnchorDisplayObject = buttonFive;
			buttonEight.layoutData = eightLD;
			
			var nineLD:AnchorLayoutData = new AnchorLayoutData(GAP, NaN, NaN, GAP);
			nineLD.leftAnchorDisplayObject = buttonEight;
			nineLD.topAnchorDisplayObject = buttonSix;
			buttonNine.layoutData = nineLD;
		}
		override public function dispose():void
		{
			buttonOne = null;
			buttonTwo = null;
			buttonThree = null;
			buttonFour = null;
			buttonFive = null;
			buttonSix = null;
			buttonSeven = null;
			buttonEight = null;
			buttonNine = null;
			backButton = null;
			super.dispose();
		}
	}
}