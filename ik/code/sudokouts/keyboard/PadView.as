package ik.code.sudokouts.keyboard
{
	import feathers.layout.AnchorLayoutData;
	
	import ik.code.sudokouts.EmbeddedAssets;
	
	public class PadView extends KeyboardView
	{
		public static const ID:int = 1;
		
		private var arrowButtonUp:KeyboardButton;
		private var arrowButtonDown:KeyboardButton;
		private var arrowButtonLeft:KeyboardButton;
		private var arrowButtonRight:KeyboardButton;
		
		private var selectButton:KeyboardButton;
		private var hintButton:KeyboardButton;
		
		private var dummyButton:KeyboardButton;
		private var dummyButton2:KeyboardButton;
		private var dummyButton3:KeyboardButton;
		private var dummyButton4:KeyboardButton;
		
		public function PadView(keyboard:Keyboard)
		{
			super(keyboard);
			
			arrowButtonUp = buttonsFactory(KeyboardButton.UP, null, EmbeddedAssets.PAD_UP_ICON_TEXTURE);
			addChild(arrowButtonUp);
			
			arrowButtonDown = buttonsFactory(KeyboardButton.DOWN, null, EmbeddedAssets.PAD_DOWN_ICON_TEXTURE);
			addChild(arrowButtonDown);
			
			arrowButtonLeft = buttonsFactory(KeyboardButton.LEFT, null, EmbeddedAssets.PAD_LEFT_ICON_TEXTURE);
			addChild(arrowButtonLeft);
			
			arrowButtonRight = buttonsFactory(KeyboardButton.RIGHT, null, EmbeddedAssets.PAD_RIGHT_ICON_TEXTURE);
			addChild(arrowButtonRight);
			
			selectButton = buttonsFactory(KeyboardButton.SELECT, null, EmbeddedAssets.PAD_SELECT_ICON_TEXTURE);
			addChild(selectButton);
			
			hintButton = buttonsFactory(KeyboardButton.HINT, null, EmbeddedAssets.HINT_ICON_TEXTURE, true, 0x69747C);
			addChild(hintButton);
			
			dummyButton = buttonsFactory(null, null, null, false);
			addChild(dummyButton);
			
			dummyButton2 = buttonsFactory(null, null, null, false);
			addChild(dummyButton2);
			
			dummyButton3 = buttonsFactory(null, null, null, false);
			addChild(dummyButton3);
			
			dummyButton4 = buttonsFactory(null, null, null, false);
			addChild(dummyButton4);
			
			var dummyLD:AnchorLayoutData = new AnchorLayoutData(NaN, NaN, NaN, extraButtonsGap);
			dummyLD.leftAnchorDisplayObject = pencilButton;
			dummyButton.layoutData = dummyLD;
			
			var arrowUpLD:AnchorLayoutData = new AnchorLayoutData(NaN, NaN, NaN, GAP);
			arrowUpLD.leftAnchorDisplayObject = dummyButton;
			arrowButtonUp.layoutData = arrowUpLD;
			
			var dummy2LD:AnchorLayoutData = new AnchorLayoutData(NaN, NaN, NaN, GAP);
			dummy2LD.leftAnchorDisplayObject = arrowButtonUp;
			dummyButton2.layoutData = dummy2LD;
			
			var arrowLeftLD:AnchorLayoutData = new AnchorLayoutData(GAP, NaN, NaN, extraButtonsGap);
			arrowLeftLD.topAnchorDisplayObject = dummyButton;
			arrowLeftLD.leftAnchorDisplayObject = pencilButton;
			arrowButtonLeft.layoutData = arrowLeftLD;
			
			var selectLD:AnchorLayoutData = new AnchorLayoutData(GAP, NaN, NaN, GAP);
			selectLD.leftAnchorDisplayObject = arrowButtonLeft;
			selectLD.topAnchorDisplayObject = arrowButtonUp;
			selectButton.layoutData = selectLD;
			
			var arrowRightLD:AnchorLayoutData = new AnchorLayoutData(GAP, NaN, NaN, GAP);
			arrowRightLD.leftAnchorDisplayObject = selectButton;
			arrowRightLD.topAnchorDisplayObject = dummyButton2;
			arrowButtonRight.layoutData = arrowRightLD;
			
			var hintLD:AnchorLayoutData = new AnchorLayoutData(GAP, NaN, NaN, NaN);
			hintLD.topAnchorDisplayObject = pencilButton;
			hintButton.layoutData = hintLD;
			
			var dummy3LD:AnchorLayoutData = new AnchorLayoutData(GAP, NaN, NaN, extraButtonsGap);
			dummy3LD.leftAnchorDisplayObject = hintButton;
			dummy3LD.topAnchorDisplayObject = arrowButtonLeft;
			dummyButton3.layoutData = dummy3LD;
			
			var arrowDownLD:AnchorLayoutData = new AnchorLayoutData(GAP, NaN, NaN, GAP);
			arrowDownLD.leftAnchorDisplayObject = dummyButton3;
			arrowDownLD.topAnchorDisplayObject = selectButton;
			arrowButtonDown.layoutData = arrowDownLD;
			
			var dummy4LD:AnchorLayoutData = new AnchorLayoutData(GAP, NaN, NaN, GAP);
			dummy4LD.leftAnchorDisplayObject = arrowButtonDown;
			dummy4LD.topAnchorDisplayObject = arrowButtonRight;
			dummyButton4.layoutData = dummy4LD;
		}
		override public function dispose():void
		{
			arrowButtonUp = null;
			arrowButtonDown = null;
			arrowButtonLeft = null;
			arrowButtonRight = null;
			
			selectButton = null;
			hintButton = null;
			
			dummyButton = null;
			dummyButton2 = null;
			dummyButton3 = null;
			dummyButton4 = null;
			super.dispose();
		}
	}
}