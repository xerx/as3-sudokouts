package ik.code.sudokouts
{
	import feathers.controls.ScreenNavigator;
	import feathers.controls.ScreenNavigatorItem;
	import feathers.motion.Fade;
	import feathers.themes.TopcoatLightMobileTheme;
	
	import ik.code.sudokouts.game.Level;
	import ik.code.sudokouts.screens.GameScreen;
	import ik.code.sudokouts.screens.MenuScreen;
	import ik.code.sudokouts.screens.SettingsScreen;
	import ik.code.sudokouts.screens.StatisticsScreen;
	
	import starling.display.DisplayObject;
	import starling.events.Event;
	
	public class Main extends ScreenNavigator
	{
		public function Main()
		{
			EmbeddedAssets.initialize();
			new TopcoatLightMobileTheme();
			super();
		}
		override protected function initialize():void
		{
			super.initialize();
			addScreen(MenuScreen.ID, new ScreenNavigatorItem(MenuScreen, {levelSelected:initGameWithLevel,
																			continueGameRequest:continueGame}));
			addScreen(GameScreen.ID, new ScreenNavigatorItem(GameScreen, {menuRequest:showMenu, 
																		  complete:gameComplete}));
			addScreen(SettingsScreen.ID, new ScreenNavigatorItem(SettingsScreen));
			addScreen(StatisticsScreen.ID, new ScreenNavigatorItem(StatisticsScreen));
			
			showScreen(MenuScreen.ID);
		}
		private function initGameWithLevel(e:Event):void
		{
			getScreen(GameScreen.ID).properties.continueGame = false;
			getScreen(GameScreen.ID).properties.level = Level.getLevelByName(e.data as String);
			showScreen(GameScreen.ID);
		}
		private function continueGame(e:Event):void
		{
			getScreen(GameScreen.ID).properties.continueGame = true;
			showScreen(GameScreen.ID);
		}
		private function gameComplete():void
		{
			showScreen(MenuScreen.ID);
		}
		private function showMenu():void
		{
			showScreen(MenuScreen.ID);
		}
		override public function showScreen(id:String, transition:Function=null):DisplayObject
		{
			if(id == GameScreen.ID)
				transition = Fade.createFadeOutTransition();
			return super.showScreen(id, transition);
		}
	}
}