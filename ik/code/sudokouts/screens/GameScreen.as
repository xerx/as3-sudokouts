package ik.code.sudokouts.screens
{
	import com.greensock.TweenLite;
	
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.Label;
	import feathers.controls.PanelScreen;
	
	import ik.code.sudokouts.EmbeddedAssets;
	import ik.code.sudokouts.Globals;
	import ik.code.sudokouts.game.GameManager;
	import ik.code.sudokouts.game.Level;
	import ik.code.sudokouts.game.SaveManager;
	import ik.code.sudokouts.game.SaveObject;
	import ik.code.sudokouts.grid.Grid;
	import ik.code.sudokouts.keyboard.Keyboard;
	import ik.code.sudokouts.keyboard.KeyboardView;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	
	public class GameScreen extends PanelScreen
	{
		public static const ID:String = "GameScreen";
		private var _level:Level;
		private var _continueGame:Boolean;
		
		private var keyboard:Keyboard;
		private var grid:Grid;
		private var gameManager:GameManager;
		
		public function GameScreen()
		{
			super();
			horizontalScrollPolicy = verticalScrollPolicy = "off";
		}
		override protected function initialize():void
		{
			super.initialize();
			
			if(_continueGame)
			{
				var saveObject:SaveObject = SaveManager.getSavedGame();
				_level = Level.getLevelByName(saveObject.level);
			}
			title = "ΣΚΟΡ\n" + (_continueGame ?saveObject.score.toString():_level.maxScore.toString());;
			
			headerFactory = function():Header
			{
				var header:Header = new Header();
				var backButton:Button = new Button();
				backButton.styleNameList.add("clearButtonStyle");
				backButton.defaultIcon = new Image(EmbeddedAssets.LEFT_ARROW_ICON_TEXTURE); 
				backButton.addEventListener(Event.TRIGGERED, backClicked);
				header.leftItems = new <DisplayObject>[backButton];
				
				var scoreLabel:Label = new Label();
				scoreLabel.fontStyles = Globals.theme.lightFontStyles;
				scoreLabel.maxWidth = 100;
				scoreLabel.wordWrap = true;
				header.rightItems = new <DisplayObject>[scoreLabel];
				scoreLabel.text = _level.name;
				
				if(_continueGame)
				{
					header.backgroundSkin = new Quad(1, 1, 0x6BAA75);
				}
				return header;
			}
			backButtonHandler = backClicked;
		}
		protected function backClicked():void
		{
			dispatchEventWith("menuRequest");
		}
		override protected function draw():void
		{
			super.draw();
			if(!keyboard)
			{
				keyboard = new Keyboard();
				keyboard.y = Starling.current.stage.stageHeight;
				addChild(keyboard);
				
				TweenLite.to(keyboard, .4, {y:Starling.current.stage.stageHeight - header.height - KeyboardView.size.height, onComplete:initGrid});
			}
		}
		private function initGrid():void
		{
			var size:int = Starling.current.stage.stageWidth > keyboard.y ? keyboard.y:Starling.current.stage.stageWidth;
			
			grid = new Grid(size, 3);
			
			gameManager = new GameManager(grid, keyboard, _level, _continueGame);
			gameManager.addEventListener(Event.COMPLETE, gameComplete);
			gameManager.addEventListener("scoreUpdated", scoreUpdate);
			
			grid.x = (Starling.current.stage.stageWidth - grid.width) * .5;
			grid.y = (keyboard.y - grid.height) * .5;
			addChild(grid);
		}
		private function scoreUpdate(e:Event):void
		{
			title = "ΣΚΟΡ\n" + e.data.toString();
		}
		private function gameComplete():void
		{
			dispatchEventWith(Event.COMPLETE);
		}
		public function set level(value:Level):void
		{
			_level = value;
		}
		public function set continueGame(value:Boolean):void
		{
			_continueGame = value;
		}
		override public function dispose():void
		{
			_level = null;
			keyboard.dispose();
			keyboard = null;
			grid.dispose();
			grid = null;
			gameManager.destroy();
			gameManager = null;
			super.dispose();
		}
	}
}