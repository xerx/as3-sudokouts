package ik.code.sudokouts.screens
{
	import feathers.controls.Screen;
	
	import ik.code.sudokouts.EmbeddedAssets;
	
	import starling.display.Image;
	
	public class BaseScreen extends Screen
	{
		public function BaseScreen()
		{
			super();
		}
		override protected function initialize():void
		{
			super.initialize();
			
			backgroundSkin = new Image(EmbeddedAssets.BACKGROUND_TEXTURE);
			backButtonHandler = backClicked;
		}
		protected function backClicked():void
		{
			
		}
	}
}