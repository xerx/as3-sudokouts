package ik.code.sudokouts.screens
{
	import com.greensock.TimelineLite;
	import com.greensock.TweenLite;
	import com.greensock.easing.Elastic;
	
	import feathers.controls.Label;
	
	import ik.code.sudokouts.EmbeddedAssets;
	import ik.code.sudokouts.controls.ContinueGameButton;
	import ik.code.sudokouts.controls.ExpandingMenuButton;
	import ik.code.sudokouts.controls.MenuButton;
	import ik.code.sudokouts.controls.NewGameButton;
	import ik.code.sudokouts.controls.SettingsButton;
	import ik.code.sudokouts.controls.StatsButton;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.events.Event;
	
	public class MenuScreen extends BaseScreen
	{
		public static const ID:String = "MenuScreen";

		private var buttons:Vector.<MenuButton>;
		private var mpampas:Image;

		private var label:Label;

		private var tmln:TimelineLite;
		private var expandedButton:ExpandingMenuButton;
		
		public function MenuScreen()
		{
			super();
		}
		override protected function initialize():void
		{
			super.initialize();
			
			width = Starling.current.stage.stageWidth;
			height = Starling.current.stage.stageHeight;
			buttons = new <MenuButton>[];
			var buttonSize:int = int(width * .45);
			var halfWidth:int = width >> 1;
			var halfHeight:int = height >> 1;
			
			mpampas = new Image(EmbeddedAssets.MPAMPAS_TEXTURE);
			mpampas.pivotX = mpampas.pivotY = int(mpampas.width * .5);
			mpampas.scaleX = mpampas.scaleY = .7;
			mpampas.x = halfWidth;
			mpampas.y = halfHeight * .25;
			mpampas.alpha = 0;
			addChild(mpampas);
			
			label = new Label();
			label.styleNameList.add(Label.ALTERNATE_STYLE_NAME_HEADING);
			label.text = "SudoKouts";
			label.validate();
			label.fontStyles.size += 4;
			label.validate();
			label.x = halfWidth - label.width * .5;
			label.y = mpampas.bounds.bottom * 1.15;
			label.alpha = 0;
			addChild(label);
			
			var newGameButton:NewGameButton = new NewGameButton(buttonSize, buttonSize, "ΝΕΟ\nΠΑΙΧΝΙΔΙ", 0x3D5A6C);
			newGameButton.addEventListener("selected", buttonSelected);
			newGameButton.addEventListener("stateUpdated", buttonStateUpdated);
			newGameButton.alpha = 0;
			addChild(newGameButton);
			buttons.push(newGameButton);
			
			var continueGameButton:ContinueGameButton = new ContinueGameButton(buttonSize, buttonSize, "ΣΥΝΕΧΙΣΗ\nΠΑΙΧΝΙΔΙΟΥ", 0x6BAA75);
			continueGameButton.addEventListener("selected", buttonSelected);
			continueGameButton.alpha = 0;
			addChild(continueGameButton);
			buttons.push(continueGameButton);
			
			var settingsButton:SettingsButton = new SettingsButton(buttonSize, buttonSize, "ΡΥΘΜΙΣΕΙΣ", 0xC03221);
			settingsButton.addEventListener("selected", buttonSelected);
			settingsButton.addEventListener("stateUpdated", buttonStateUpdated);
			settingsButton.alpha = 0;
			addChild(settingsButton);
			buttons.push(settingsButton);
			
			var statsButton:StatsButton = new StatsButton(buttonSize, buttonSize, "ΣΤΑΤΙΣΤΙΚΑ", 0x69747C);
			statsButton.addEventListener("selected", buttonSelected);
			statsButton.addEventListener("stateUpdated", buttonStateUpdated);
			statsButton.alpha = 0;
			addChild(statsButton);
			buttons.push(statsButton);
			
			var leftX:Number = halfWidth - buttonSize - MenuButton.GAP;
			var upY:Number = label.bounds.bottom * 1.1;
			var rightX:Number = halfWidth  + MenuButton.GAP;
			var downY:Number = upY + buttonSize + MenuButton.GAP;
			
			newGameButton.y = upY;
			continueGameButton.x = rightX;
			continueGameButton.y = upY - leftX; 
			settingsButton.x = leftX;
			settingsButton.y = downY + leftX;
			settingsButton.setDefaultPosition(leftX, downY);
			statsButton.x = width - buttonSize;
			statsButton.y = downY;
			statsButton.setDefaultPosition(rightX, downY);
			mpampas.scaleX = mpampas.scaleY = 0;
			
			tmln = new TimelineLite();
			tmln.append(new TweenLite(newGameButton, .3, {x:leftX, alpha:1}));
			tmln.append(new TweenLite(continueGameButton, .3, {y:upY, alpha:continueGameButton.isEnabled ? 1:.5, delay:-.1}));
			tmln.append(new TweenLite(statsButton, .3, {x:rightX, alpha:1, delay:-.1}));
			tmln.append(new TweenLite(settingsButton, .3, {y:downY, alpha:1, delay:-.1}));
			tmln.append(new TweenLite(mpampas, 1, {scaleX:.7, scaleY:.7, alpha:1, ease:Elastic.easeInOut}));
			tmln.append(new TweenLite(label, .4, {alpha:1}));
			tmln.play();
		}
		
		private function buttonStateUpdated(e:Event):void
		{
			expandedButton = e.currentTarget as ExpandingMenuButton;
			if(expandedButton.getState() == ExpandingMenuButton.DEFAULT)
				expandedButton = null;
		}
		
		private function buttonSelected(e:Event):void
		{
			for (var i:int = 0; i < buttons.length; i++) 
			{
				if(e.currentTarget != buttons[i])
					buttons[i].visible = false;
			}
			mpampas.visible = label.visible = false;
		}
		
		override protected function backClicked():void
		{
			if(expandedButton)
			{
				expandedButton.setState(ExpandingMenuButton.DEFAULT);
			}
		}
		override public function dispose():void
		{
			for (var i:int = 0; i < buttons.length; i++) 
			{
				buttons[i].removeEventListener("selected", buttonSelected);
				buttons[i].removeEventListener("stateUpdated", buttonStateUpdated);
			}
			expandedButton = null;
			tmln.kill();
			buttons = null;
			super.dispose();
		}
	}
}