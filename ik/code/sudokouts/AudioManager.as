package ik.code.sudokouts
{
	import flash.media.Sound;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	
	import ik.code.sudokouts.game.SaveManager;

	public class AudioManager
	{
		private static var transform:SoundTransform = new SoundTransform(.6);
		
		public static function play(sound:String):void
		{
			if(SaveManager.isMuted())return;
			var snd:Sound = new Sound(new URLRequest("../../sounds/" + sound + ".mp3")); 
			snd.play(0, 0, transform);
		}
	}
}