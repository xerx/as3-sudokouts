package ik.code.sudokouts
{
	import starling.textures.Texture;

	public class EmbeddedAssets
	{
		[Embed(source="../../../../assets/images/icons/left-arrow-icon.png")]
		private static const LEFT_ARROW_ICON_CLASS:Class;
		public static var LEFT_ARROW_ICON_TEXTURE:Texture;
		
		[Embed(source="../../../../assets/images/icons/hint-icon.png")]
		private static const HINT_ICON_CLASS:Class;
		public static var HINT_ICON_TEXTURE:Texture;
		
		[Embed(source="../../../../assets/images/icons/pencil-icon.png")]
		private static const PENCIL_ICON_CLASS:Class;
		public static var PENCIL_ICON_TEXTURE:Texture;
		
		[Embed(source="../../../../assets/images/icons/pad-left-icon.png")]
		private static const PAD_LEFT_ICON_CLASS:Class;
		public static var PAD_LEFT_ICON_TEXTURE:Texture;
		
		[Embed(source="../../../../assets/images/icons/pad-right-icon.png")]
		private static const PAD_RIGHT_ICON_CLASS:Class;
		public static var PAD_RIGHT_ICON_TEXTURE:Texture;
		
		[Embed(source="../../../../assets/images/icons/pad-up-icon.png")]
		private static const PAD_UP_ICON_CLASS:Class;
		public static var PAD_UP_ICON_TEXTURE:Texture;
		
		[Embed(source="../../../../assets/images/icons/pad-down-icon.png")]
		private static const PAD_DOWN_ICON_CLASS:Class;
		public static var PAD_DOWN_ICON_TEXTURE:Texture;
		
		[Embed(source="../../../../assets/images/icons/pad-select-icon.png")]
		private static const PAD_SELECT_ICON_CLASS:Class;
		public static var PAD_SELECT_ICON_TEXTURE:Texture;
		
		[Embed(source="../../../../assets/images/mpampas.png")]
		private static const MPAMPAS_CLASS:Class;
		public static var MPAMPAS_TEXTURE:Texture;
		
		[Embed(source="../../../../assets/images/background.jpg")]
		private static const BACKGROUND_CLASS:Class;
		public static var BACKGROUND_TEXTURE:Texture;
		
		public static function initialize():void
		{
			LEFT_ARROW_ICON_TEXTURE = Texture.fromEmbeddedAsset(LEFT_ARROW_ICON_CLASS, false, false, 3);
			HINT_ICON_TEXTURE = Texture.fromEmbeddedAsset(HINT_ICON_CLASS, false, false, 2);
			PENCIL_ICON_TEXTURE = Texture.fromEmbeddedAsset(PENCIL_ICON_CLASS, false, false, 2);
			PAD_LEFT_ICON_TEXTURE = Texture.fromEmbeddedAsset(PAD_LEFT_ICON_CLASS, false, false, 2);
			PAD_RIGHT_ICON_TEXTURE = Texture.fromEmbeddedAsset(PAD_RIGHT_ICON_CLASS, false, false, 2);
			PAD_UP_ICON_TEXTURE = Texture.fromEmbeddedAsset(PAD_UP_ICON_CLASS, false, false, 2);
			PAD_DOWN_ICON_TEXTURE = Texture.fromEmbeddedAsset(PAD_DOWN_ICON_CLASS, false, false, 2);
			PAD_SELECT_ICON_TEXTURE = Texture.fromEmbeddedAsset(PAD_SELECT_ICON_CLASS, false, false, 2);
			MPAMPAS_TEXTURE = Texture.fromEmbeddedAsset(MPAMPAS_CLASS, false, false, 2);
			BACKGROUND_TEXTURE = Texture.fromEmbeddedAsset(BACKGROUND_CLASS, false, false, 2);
		}
	}
}