package ik.code.sudokouts.game
{
	public class SaveObject
	{

		public var level:String;

		public var score:int;

		public var boxes:Array;

		public function SaveObject(level:String, score:int, boxes:Array)
		{
			this.boxes = boxes;
			this.score = score;
			this.level = level;
		}
		public function data():Object
		{
			return {
				level:this.level,
				score:this.score,
				boxes:this.boxes
			}
		}
	}
}