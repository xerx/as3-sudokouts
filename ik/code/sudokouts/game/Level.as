package ik.code.sudokouts.game
{
	public class Level
	{
		
		private static var levels:Vector.<Level> = new <Level>[];
		
		public static const EASY:Level = new Level("ΕΥΚΟΛΟ", 70, 30, 5, 30);
		public static const MEDIUM:Level = new Level("ΚΑΝΟΝΙΚΟ", 150, 40, 30, 26);
		public static const HARD:Level = new Level("ΔΥΣΚΟΛΟ", 300, 60, 50, 24);
		public static const VERY_HARD:Level = new Level("ΠΟΛΥ ΔΥΣΚΟΛΟ", 400, 50, 70, 22);
		
		public var name:String;
		public var maxScore:int;
		public var minScore:int;
		public var penaltyPoints:int;
		public var givenBlocks:int;
		
		public function Level(name:String, maxScore:int, minScore:int, penaltyPoints:int, givenBlocks:int)
		{
			this.givenBlocks = givenBlocks;
			this.penaltyPoints = penaltyPoints;
			this.minScore = minScore;
			this.maxScore = maxScore;
			this.name = name;

			levels.push(this);
		}
		public static function getLevelByName(name:String):Level
		{
			for(var i:int = 0; i < levels.length; i++) 
			{
				if(name == levels[i].name)
					return levels[i];
			}
			return null;			
		}
	}
}