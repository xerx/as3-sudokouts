package ik.code.sudokouts.game
{
	public class StatsObject
	{
		private const SECOND:int = 1000;
		private const MINUTE:int = 60 * SECOND;
		private const HOUR:int = 60 * MINUTE;
		private const DAY:int = 24 * HOUR;
		
		public var totalGamesPlayed:uint;
		public var totalPoints:uint;
		public var numPerfectGames:uint;
		public var totalGameTime:uint;
		
		public function StatsObject(totalGamesPlayed:uint = 0, 
									totalPoints:uint = 0, 
									numPerfectGames:uint = 0,
									totalGameTime:uint = 0)
		{
			this.totalGameTime = totalGameTime;
			this.totalGamesPlayed = totalGamesPlayed;
			this.totalPoints = totalPoints;
			this.numPerfectGames = numPerfectGames;
		}
		public function addStats(stats:StatsObject):void
		{
			totalGamesPlayed += stats.totalGamesPlayed;
			totalPoints += stats.totalPoints;
			numPerfectGames += stats.numPerfectGames;
			totalGameTime += stats.totalGameTime;
		}
		public function getTime():String
		{
			if(totalGameTime < MINUTE)
			{
				return Math.floor(totalGameTime / SECOND) + " δευτ/λεπτα";
			}
			if(totalGameTime < HOUR)
			{
				var seconds:Number = (totalGameTime % MINUTE) / SECOND;
				return Math.floor(totalGameTime / MINUTE) + " λεπτά" + (seconds > 0 ? " & " + Math.floor(seconds) + " δευτ.":"");
			}
			if(totalGameTime < DAY)
			{
				var minutes:Number = (totalGameTime % HOUR) / MINUTE;
				return Math.floor(totalGameTime / HOUR) + " ώρες" + (minutes > 0 ? " & " + Math.floor(minutes) + " λεπτά":"");
			}
			
			var hours:Number = (totalGameTime % DAY) / HOUR;
			return Math.floor(totalGameTime / DAY) + " μέρες" + (hours > 0 ? " & " + Math.floor(hours) + " ώρες":"");
		}
		public function fromObject(object:Object):void
		{
			this.totalGamesPlayed = object.totalGamesPlayed;
			this.totalPoints = object.totalPoints;
			this.numPerfectGames = object.numPerfectGames;
			this.totalGameTime = object.totalGameTime;
		}
		public function toObject():Object
		{
			return {
				totalGamesPlayed:this.totalGamesPlayed,
					totalPoints:this.totalPoints,
					numPerfectGames:this.numPerfectGames,
					totalGameTime:this.totalGameTime
			}
		}
	}
}