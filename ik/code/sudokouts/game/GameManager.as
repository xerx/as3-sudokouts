package ik.code.sudokouts.game
{
	import flash.utils.getTimer;
	
	import feathers.controls.Alert;
	import feathers.data.ListCollection;
	
	import ik.code.sudokouts.AudioManager;
	import ik.code.sudokouts.grid.Grid;
	import ik.code.sudokouts.keyboard.Keyboard;
	import ik.code.sudokouts.keyboard.KeyboardButton;
	import ik.code.sudokouts.keyboard.NumericView;
	import ik.code.sudokouts.keyboard.PadView;
	
	import starling.events.Event;
	import starling.events.EventDispatcher;

	public class GameManager extends EventDispatcher
	{
		private var grid:Grid;
		private var keyboard:Keyboard;
		private var level:Level;
		private var score:int;
		private var gameTime:uint;
		
		public function GameManager(grid:Grid, keyboard:Keyboard, level:Level, continueGame:Boolean)
		{
			this.keyboard = keyboard;
			this.grid = grid;
			this.level = level;
			
			grid.build(continueGame ? SaveManager.getSavedGame():null);
			
			if(grid.getLevel() == null)
				grid.setLevel(level);
			
			keyboard.addEventListener("buttonClicked", keyboardButtonClicked);
			
			score = level.maxScore;
			gameTime = getTimer();
			saveGame();
			
			Alert.alertFactory = function():Alert
			{
				var alert:Alert = new Alert();
				alert.outerPadding = 5;
				alert.gap = 5;
				return alert;
			}
		}
		private function keyboardButtonClicked(e:Event):void
		{
			var keyID:String = e.data as String;
			if(keyID == KeyboardButton.UP || keyID == KeyboardButton.DOWN ||
			   keyID == KeyboardButton.LEFT || keyID == KeyboardButton.RIGHT)
			{
				grid.focusWithDirection(e.data as String);
			}
			else if(keyID == KeyboardButton.SELECT)
			{
				if(grid.focusedBoxIsActive)
					keyboard.setView(NumericView.ID);
			}
			else if(keyID == KeyboardButton.HINT)
			{
				grid.focusOnHint();
			}
			else if(!isNaN(Number(keyID)))
			{
				var isCorrect:Boolean = grid.userInput(keyID, keyboard.pencilIsActive);
				keyboard.setView(PadView.ID);
				
				if(isCorrect)
				{
					if(grid.isComplete)
					{
						SaveManager.clearSavedGame();
						SaveManager.updateStatsWith(new StatsObject(1, score, level.maxScore == score ? 1:0, getTimer() - gameTime));
						gameTime = 0;
						
						var alert:Alert = Alert.show("ΣΚΟΡ: " + String(score), "ΩΡΑΙΟΣ ΠΑΤΕΡΑ", new ListCollection([{label:"OK"}]));
						alert.addEventListener(Event.CLOSE, alertClosed);
					}
					else
					{
						saveGame();
					}
					AudioManager.play("correct");
				}
				else if(!keyboard.pencilIsActive)
				{
					if(score > level.minScore)
					{
						score -= level.penaltyPoints;
						saveGame();
						dispatchEventWith("scoreUpdated", false, score);
					}
					AudioManager.play("wrong");
				}
			}
		}
		
		private function alertClosed():void
		{
			dispatchEventWith(Event.COMPLETE);
		}
		private function saveGame():void
		{
			SaveManager.saveGame(new SaveObject(
					level.name,
					score,
					grid.boxesSaveData()
				)
			);
		}
		public function destroy():void
		{
			grid = null;
			keyboard = null;
			SaveManager.updateStatsWith(new StatsObject(0, 0, 0, getTimer() - gameTime));
		}
	}
}