package ik.code.sudokouts.game
{
	import flash.net.SharedObject;

	public class SaveManager
	{
		private static var so:SharedObject = SharedObject.getLocal("sudokouts");
		
		private static var stats:StatsObject;
		
		public static function getStats():StatsObject
		{
			if(!stats)
			{
				stats = new StatsObject();
				stats.fromObject(getRawStats());
			}
			
			return stats;
		}
		private static function getRawStats():Object
		{
			if(!so.data.stats)
			{
				so.data.stats = (new StatsObject()).toObject();
				so.flush();
			}
			return so.data.stats;
		}
		public static function updateStatsWith(newStats:StatsObject):void
		{
			getStats().addStats(newStats);
			so.data.stats = stats.toObject();
			so.flush();
		}
		public static function clearStats():void
		{
			so.data.stats = null;
			stats = null;
			so.flush();
		}
		public static function saveGame(saveObject:SaveObject):void
		{
			so.data.savedGame = saveObject.data();
			so.flush();
		}
		public static function getSavedGame():SaveObject
		{
			if(!so.data.savedGame)
				return null;
			
			var rawData:Object = so.data.savedGame; 
			
			return new SaveObject(
					rawData.level,
					rawData.score,
					rawData.boxes
				);
		}
		public static function clearSavedGame():void
		{
			so.data.savedGame = null;
			so.flush();
		}
		public static function setIsMuted(value:Boolean):void
		{
			so.data.isMuted = value;
		}
		public static function isMuted():Boolean
		{
			return Boolean(so.data.isMuted);
		}
		
	}
}

