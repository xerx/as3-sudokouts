package ik.code.sudokouts.controls
{
	import com.greensock.TweenLite;
	import com.greensock.easing.Elastic;
	
	import feathers.controls.Label;
	import feathers.layout.AnchorLayoutData;
	import feathers.layout.HorizontalAlign;
	
	import ik.code.sudokouts.AudioManager;
	import ik.code.sudokouts.game.SaveManager;
	import ik.code.sudokouts.game.StatsObject;
	
	import starling.display.DisplayObject;

	public class StatsButton extends ExpandingMenuButton
	{
		private var titleLabelTotalGames:Label;
		private var valueLabelTotalGames:Label;
		private var valueLabelTotalPoints:Label;
		private var titleLabelTotalPoints:Label;
		private var titleLabelPerfectGames:Label;
		private var valueLabelPerfectGames:Label;
		private var titleLabelGameTime:Label;
		private var valueLabelGameTime:Label;
		
		public function StatsButton(w:int, h:int, text:String, color:uint, buttonsPadding:int = 4)
		{
			super(w, h, text, color, buttonsPadding);
		}
		override protected function initialize():void
		{
			super.initialize();
			
			var paddingSide:int = int(width * .03);
			
			titleLabelTotalGames = labelsFactory(true, "ΟΛΟΚΛΗΡΩΜΕΝΑ ΠΑΙΧΝΙΔΙΑ", paddingSide * 4, paddingSide);
			addChild(titleLabelTotalGames);
			
			valueLabelTotalGames = labelsFactory(false, "", paddingSide, paddingSide, titleLabelTotalGames);
			addChild(valueLabelTotalGames);
			
			titleLabelPerfectGames = labelsFactory(true, "ΑΛΑΝΘΑΣΤΑ ΠΑΙΧΝΙΔΙΑ", paddingSide * 2, paddingSide, valueLabelTotalGames);
			addChild(titleLabelPerfectGames);
			
			valueLabelPerfectGames = labelsFactory(false, "", paddingSide, paddingSide, titleLabelPerfectGames);
			addChild(valueLabelPerfectGames);
			
			titleLabelTotalPoints = labelsFactory(true, "ΣΥΝΟΛΟ ΠΟΝΤΩΝ", paddingSide * 2, paddingSide, valueLabelPerfectGames);
			addChild(titleLabelTotalPoints);
			
			valueLabelTotalPoints = labelsFactory(false, "", paddingSide, paddingSide, titleLabelTotalPoints);
			addChild(valueLabelTotalPoints);
			
			titleLabelGameTime = labelsFactory(true, "ΧΡΟΝΟΣ ΠΑΙΧΝΙΔΙΟΥ", paddingSide * 2, paddingSide, valueLabelTotalPoints);
			addChild(titleLabelGameTime);
			
			valueLabelGameTime = labelsFactory(false, "", paddingSide, paddingSide, titleLabelGameTime);
			addChild(valueLabelGameTime);
		}
		private function labelsFactory(isTitle:Boolean, text:String, paddingTop:int, paddingSide:int, topAnchor:DisplayObject = null):Label
		{
			var newLabel:Label = new Label();
			newLabel.validate();
			newLabel.fontStyles.horizontalAlign = HorizontalAlign.CENTER;
			newLabel.fontStyles.color = isTitle ? 0xFFFFFF:0x83C763;
			newLabel.text = text;
			newLabel.visible = false;
			
			var ld:AnchorLayoutData = new AnchorLayoutData(paddingTop, paddingSide, NaN, paddingSide, 0);
			if(topAnchor)ld.topAnchorDisplayObject = topAnchor;
			newLabel.layoutData = ld;
			return newLabel;
		}
		override public function setState(value:int):void
		{
			super.setState(value);
			
			if(state == DEFAULT)
			{
				titleLabelTotalGames.visible = false;
				valueLabelTotalGames.visible = false;
				valueLabelTotalPoints.visible = false;
				titleLabelTotalPoints.visible = false;
				titleLabelPerfectGames.visible = false;
				valueLabelPerfectGames.visible = false;
				titleLabelGameTime.visible = false;
				valueLabelGameTime.visible = false;
				
				AudioManager.play("menu_collapse");
				TweenLite.to(this, TWEEN_DURATION, {x:defaultPosition.x,
													y:defaultPosition.y,
													width:int(width * .5) - GAP, 
													height:int(height * .5) - GAP, 
													ease:Elastic.easeOut, onComplete:stateUpdated});
			}
			else
			{
				
				AudioManager.play("menu_expand");
				TweenLite.to(this, TWEEN_DURATION, {x:defaultPosition.x - width - GAP * 2,
													y:defaultPosition.y - height - GAP * 2,
												    width:int(width * 2) + GAP * 2, 
													height:int(height * 2) + GAP * 2, 
													ease:Elastic.easeOut, onComplete:stateUpdated});
			}
		}
		override protected function stateUpdated():void
		{
			super.stateUpdated();
			
			if(state == SUB_MENU)
			{
				var stats:StatsObject = SaveManager.getStats();
				valueLabelTotalGames.text = String(stats.totalGamesPlayed);
				valueLabelPerfectGames.text = String(stats.numPerfectGames);
				valueLabelTotalPoints.text = String(stats.totalPoints);
				valueLabelGameTime.text = stats.getTime();
				
				titleLabelTotalGames.visible = true;
				valueLabelTotalGames.visible = true;
				valueLabelTotalPoints.visible = true;
				titleLabelTotalPoints.visible = true;
				titleLabelPerfectGames.visible = true;
				valueLabelPerfectGames.visible = true;
				titleLabelGameTime.visible = true;
				valueLabelGameTime.visible = true;
				
				titleLabelTotalGames.maxWidth = width;
				valueLabelTotalGames.maxWidth = width;
				valueLabelTotalPoints.maxWidth = width;
				titleLabelTotalGames.maxWidth = width;
				titleLabelPerfectGames.maxWidth = width;
				valueLabelPerfectGames.maxWidth = width;
				titleLabelGameTime.maxWidth = width;
				valueLabelGameTime.maxWidth = width;
			}
		}
		override public function dispose():void
		{
			titleLabelTotalGames = null;
			valueLabelTotalGames = null;
			valueLabelTotalPoints = null;
			titleLabelTotalPoints = null;
			titleLabelPerfectGames = null;
			valueLabelPerfectGames = null;
			titleLabelGameTime = null;
			valueLabelGameTime = null;
			super.dispose();
		}
	}
}