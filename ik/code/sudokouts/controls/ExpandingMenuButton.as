package ik.code.sudokouts.controls
{
	import com.greensock.TweenLite;
	
	import flash.geom.Point;
	
	import feathers.controls.Button;
	import feathers.layout.AnchorLayoutData;
	
	import ik.code.sudokouts.EmbeddedAssets;
	
	import starling.display.Image;
	import starling.events.Event;

	public class ExpandingMenuButton extends MenuButton
	{
		public static const DEFAULT:int = 1;
		public static const SUB_MENU:int = 2;
		
		protected var state:int;
		
		protected var backButton:Button;
		protected var buttonsPadding:int;
		
		protected var defaultPosition:Point;
		
		public function ExpandingMenuButton(w:int, h:int, text:String, color:uint, buttonsPadding:int)
		{
			super(w, h, text, color);
			this.buttonsPadding = buttonsPadding;
			state = DEFAULT;
		}
		override protected function initialize():void
		{
			super.initialize();
			
			backButton = buttonsFactory();
			backButton.defaultIcon = new Image(EmbeddedAssets.LEFT_ARROW_ICON_TEXTURE); 
			backButton.addEventListener(Event.TRIGGERED, buttonClicked);
			addChild(backButton);
		}
		protected function buttonClicked(e:Event):void
		{
			if(e.currentTarget == backButton)
			{
				setState(DEFAULT);
			}
			else
			{
				backButton.visible = false;
				dispatchEventWith("selected");
			}
			
		}
		
		override protected function triggered():void
		{
			setState(SUB_MENU); 
		}
		public function setState(value:int):void
		{
			if(state == value)
				return;
			
			state = value;
			
			if(state == DEFAULT)
			{
				backButton.visible = false;
			}
			else
			{
				trigger.isEnabled = false;
				label.visible = false;
				parent.addChild(this);
			}
		}
		protected function stateUpdated():void
		{
			if(state == SUB_MENU)
			{
				backButton.visible = true;
				backButton.layoutData = new AnchorLayoutData(NaN, NaN, 4, NaN, 0);
			}
			else
			{
				trigger.isEnabled = true;
				label.visible = true;
			}
			dispatchEventWith("stateUpdated");
		}
		protected function buttonsFactory(label:String = ""):Button
		{
			var button:Button = new Button();
			button.styleNameList.add("clearButtonStyle");
			button.visible = false;
			button.label = label;
			return button;
		}
		public function setDefaultPosition(xx:Number, yy:Number):void
		{
			defaultPosition = new Point(xx, yy);
		}
		public function getState():int
		{
			return state;
		}
		override public function dispose():void
		{
			TweenLite.killTweensOf(this);
			backButton = null;
			defaultPosition = null;
			super.dispose();
		}
	}
}