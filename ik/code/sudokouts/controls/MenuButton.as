package ik.code.sudokouts.controls
{
	import feathers.controls.Label;
	import feathers.controls.ScrollContainer;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import feathers.layout.HorizontalAlign;
	import feathers.utils.touch.TapToTrigger;
	
	import starling.display.Quad;
	import starling.events.Event;
	
	public class MenuButton extends ScrollContainer
	{
		public static const GAP:int = 1;
		protected static const TWEEN_DURATION:Number = .8;
		
		protected var label:Label;
		protected var trigger:TapToTrigger;
		
		public function MenuButton(w:int, h:int, text:String, color:uint)
		{
			super();
			
			width = w;
			height = h;
			
			label = new Label();
			label.text = text;
			label.validate();
			label.fontStyles.horizontalAlign = HorizontalAlign.CENTER;
			label.fontStyles.color = 0xFFFFFF;
			
			backgroundSkin = new Quad(width, height, color);
			
			trigger = new TapToTrigger(this);
			addEventListener(Event.TRIGGERED, triggered);
		}
		
		override protected function initialize():void
		{
			super.initialize();
			
			layout = new AnchorLayout();
			
			var labelPadding:int = int(width * .1);
			label.layoutData = new AnchorLayoutData(NaN, labelPadding, NaN, labelPadding, 0, 0);
			addChild(label);
		}
		protected function triggered():void
		{
			dispatchEventWith("selected");
		}
		override public function dispose():void
		{
			label = null;
			trigger = null;
			super.dispose();
		}
		
	}
}