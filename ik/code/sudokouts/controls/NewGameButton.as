package ik.code.sudokouts.controls
{
	import com.greensock.TweenLite;
	import com.greensock.easing.Elastic;
	
	import feathers.controls.Button;
	import feathers.layout.AnchorLayoutData;
	
	import ik.code.sudokouts.AudioManager;
	import ik.code.sudokouts.game.Level;
	
	import starling.events.Event;

	public class NewGameButton extends ExpandingMenuButton
	{		
		private var easyButton:Button;
		private var mediumButton:Button;
		private var hardButton:Button;
		private var veryHardButton:Button;
		
		public function NewGameButton(w:int, h:int, text:String, color:uint, buttonsPadding:int = 8)
		{
			super(w, h, text, color, buttonsPadding);
		}
		override protected function initialize():void
		{
			super.initialize();
			
			hardButton = buttonsFactory(Level.HARD.name);
			hardButton.addEventListener(Event.TRIGGERED, buttonClicked);
			hardButton.layoutData = new AnchorLayoutData(NaN, NaN, NaN, NaN, 0, 0);
			addChild(hardButton);
			
			mediumButton = buttonsFactory(Level.MEDIUM.name);
			mediumButton.addEventListener(Event.TRIGGERED, buttonClicked);
			var mediumLD:AnchorLayoutData = new AnchorLayoutData(NaN, NaN, buttonsPadding, NaN, 0);
			mediumLD.bottomAnchorDisplayObject = hardButton;
			mediumButton.layoutData = mediumLD;
			addChild(mediumButton);
			
			easyButton = buttonsFactory(Level.EASY.name);
			easyButton.addEventListener(Event.TRIGGERED, buttonClicked);
			var easyLD:AnchorLayoutData = new AnchorLayoutData(NaN, NaN, buttonsPadding, NaN, 0);
			easyLD.bottomAnchorDisplayObject = mediumButton;
			easyButton.layoutData = easyLD;
			addChild(easyButton);
			
			veryHardButton = buttonsFactory(Level.VERY_HARD.name);
			veryHardButton.addEventListener(Event.TRIGGERED, buttonClicked);
			var vhardLD:AnchorLayoutData = new AnchorLayoutData(buttonsPadding, NaN, NaN, NaN, 0);
			vhardLD.topAnchorDisplayObject = hardButton;
			veryHardButton.layoutData = vhardLD;
			addChild(veryHardButton);
		}
		
		override protected function buttonClicked(e:Event):void
		{
			super.buttonClicked(e);
			if(e.currentTarget != backButton)
			{
				easyButton.visible = false;
				mediumButton.visible = false;
				hardButton.visible = false;
				veryHardButton.visible = false;
				AudioManager.play("titlebar_throw");
				TweenLite.to(this, TWEEN_DURATION, {x:0, y:0, width:parent.width, height:50, ease:Elastic.easeOut, onComplete:levelSelected, onCompleteParams:[(e.currentTarget as Button).label]});
			}
				
		}
		private function levelSelected(level:String):void
		{
			dispatchEventWith("levelSelected", true, level);
		}
		override public function setState(value:int):void
		{
			if(state == value)
				return;
			
			super.setState(value);
			
			if(state == DEFAULT)
			{
				easyButton.visible = false;
				mediumButton.visible = false;
				hardButton.visible = false;
				veryHardButton.visible = false;
				AudioManager.play("menu_collapse");
				TweenLite.to(this, TWEEN_DURATION, {width:int(width * .5) - GAP, height:int(height * .5) - GAP, ease:Elastic.easeOut, onComplete:stateUpdated});
			}
			else
			{
				
				AudioManager.play("menu_expand");
				TweenLite.to(this, TWEEN_DURATION, {width:int(width * 2) + GAP * 2, height:int(height * 2) + GAP * 2, ease:Elastic.easeOut, onComplete:stateUpdated});
			}
		}
		override protected function stateUpdated():void
		{
			if(state == SUB_MENU)
			{
				hardButton.visible = true;
				mediumButton.visible = true;
				easyButton.visible = true;
				veryHardButton.visible = true;
				backButton.visible = true;
				backButton.layoutData = new AnchorLayoutData(NaN, NaN, 4, NaN, 0);
			}
			else
			{
				trigger.isEnabled = true;
				label.visible = true;
			}
			dispatchEventWith("stateUpdated");
		}
		override public function dispose():void
		{
			easyButton.removeEventListener(Event.TRIGGERED, buttonClicked);
			mediumButton.removeEventListener(Event.TRIGGERED, buttonClicked);
			hardButton.removeEventListener(Event.TRIGGERED, buttonClicked);
			veryHardButton.removeEventListener(Event.TRIGGERED, buttonClicked);
			easyButton = null;
			mediumButton = null;
			hardButton = null;
			veryHardButton = null;
			super.dispose();
		}
	}
}