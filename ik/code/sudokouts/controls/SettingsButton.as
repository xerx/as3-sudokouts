package ik.code.sudokouts.controls
{
	import com.greensock.TweenLite;
	import com.greensock.easing.Elastic;
	
	import feathers.controls.Alert;
	import feathers.controls.Button;
	import feathers.data.ListCollection;
	import feathers.layout.AnchorLayoutData;
	
	import ik.code.sudokouts.AudioManager;
	import ik.code.sudokouts.game.SaveManager;
	
	import starling.events.Event;

	public class SettingsButton extends ExpandingMenuButton
	{
		private var soundsButton:Button;
		private var resetButton:Button;
		
		public function SettingsButton(w:int, h:int, text:String, color:uint, buttonsPadding:int = 4)
		{
			super(w, h, text, color, buttonsPadding);
		}
		override protected function initialize():void
		{
			super.initialize();
			
			soundsButton = buttonsFactory(SaveManager.isMuted() ? "ΗΧΟΙ ΑΝΕΝΕΡΓΟΙ":"ΗΧΟΙ ΕΝΕΡΓΟΙ");
			soundsButton.addEventListener(Event.TRIGGERED, buttonClicked);
			soundsButton.layoutData = new AnchorLayoutData(NaN, NaN, NaN, NaN, 0, -30);
			addChild(soundsButton);
			
			resetButton = buttonsFactory("ΔΙΑΓΡΑΦΗ\nΣΤΑΤΙΣΤΙΚΩΝ");
			resetButton.addEventListener(Event.TRIGGERED, buttonClicked);
			resetButton.layoutData = new AnchorLayoutData(NaN, NaN, NaN, NaN, 0, 30);
			addChild(resetButton);
		}
		override public function setState(value:int):void
		{
			super.setState(value);
			
			if(state == DEFAULT)
			{
				soundsButton.visible = false;
				resetButton.visible = false;
				
				AudioManager.play("menu_collapse");
				TweenLite.to(this, TWEEN_DURATION, {y:defaultPosition.y,
													width:int(width * .5) - GAP, 
													height:int(height * .5) - GAP, 
													ease:Elastic.easeOut, onComplete:stateUpdated});
			}
			else
			{
				
				AudioManager.play("menu_expand");
				TweenLite.to(this, TWEEN_DURATION, {y:defaultPosition.y - height - GAP * 2,
												    width:int(width * 2) + GAP * 2, 
													height:int(height * 2) + GAP * 2, 
													ease:Elastic.easeOut, onComplete:stateUpdated});
			}
		}
		override protected function buttonClicked(e:Event):void
		{
			if(e.currentTarget == resetButton)
			{
				var alert:Alert = Alert.show("ΝΑ ΔΙΑΓΡΑΦΟΥΝ ΟΛΑ ΤΑ ΣΤΑΤΙΣΤΙΚΑ;", "ΠΡΟΣΟΧΗ", new ListCollection([{label:"ΝΑΙ"},{label:"ΟΧΙ"}]));
				alert.addEventListener(Event.CLOSE, alertClosed);
			}
			else if(e.currentTarget == soundsButton)
			{
				SaveManager.setIsMuted(!SaveManager.isMuted());
				soundsButton.label = SaveManager.isMuted() ? "ΗΧΟΙ ΑΝΕΝΕΡΓΟΙ":"ΗΧΟΙ ΕΝΕΡΓΟΙ";
			}
			else
			{
				super.buttonClicked(e);
			}
		}
		private function alertClosed(e:Event):void
		{
			if(e.data.label == "ΝΑΙ")
			{
				SaveManager.clearStats();
			}
		}
		override protected function stateUpdated():void
		{
			super.stateUpdated();
			
			if(state == SUB_MENU)
			{
				soundsButton.visible = true;
				resetButton.visible = true;
			}
		}
		override public function dispose():void
		{
			resetButton = null;
			super.dispose();
		}
	}
}