package ik.code.sudokouts.controls
{
	import com.greensock.TweenLite;
	import com.greensock.easing.Elastic;
	
	import ik.code.sudokouts.AudioManager;
	import ik.code.sudokouts.game.SaveManager;

	public class ContinueGameButton extends MenuButton
	{
		public function ContinueGameButton(w:int, h:int, text:String, color:uint)
		{
			super(w, h, text, color);
			if(!SaveManager.getSavedGame())
				isEnabled = false;
		}
		override protected function triggered():void
		{
			if(!isEnabled)return;
			super.triggered();
			
			label.visible = false;
			AudioManager.play("titlebar_throw");
			TweenLite.to(this, TWEEN_DURATION, {x:0, y:0, width:parent.width, height:50, ease:Elastic.easeOut, onComplete:finalize});
		}
		private function finalize():void
		{
			dispatchEventWith("continueGameRequest", true);
		}
	}
}