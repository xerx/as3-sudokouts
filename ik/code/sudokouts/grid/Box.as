package ik.code.sudokouts.grid
{
	import feathers.controls.Label;
	
	import ik.code.sudokouts.grid.data.BoxData;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	
	public class Box extends Sprite
	{		
		private var background:Quad;
		private var label:Label;
		private var isActive:Boolean;
		
		public var isPenciled:Boolean;
		
		private var _data:BoxData;
		private var _view:BoxView;
		
		public function Box()
		{
			super();
			background = new Quad(1, 1);
			addChild(background);
			
			label = new Label();
			label.validate();
			label.fontStyles.horizontalAlign = "center";
			addChild(label);
			
			view = BoxView.DEFAULT;
			active = true;
			labelVisible = false;
		}
		public function set view(value:BoxView):void
		{
			_view = value;
			background.color = _view.color;
			label.fontStyles.color = _view.fontColor;
			
			if(_view == BoxView.PENCIL)
				isPenciled = true;
			else if(_view == BoxView.WRONG)
				isPenciled = false;
		}
		public function get view():BoxView
		{
			return _view;
		}
		public function set active(value:Boolean):void
		{
			isActive = value;
		}
		public function get active():Boolean
		{
			return isActive;
		}
		public function set labelVisible(value:Boolean):void
		{
			label.visible = value;
		}
		public function set size(value:int):void
		{
			background.width = background.height = value;
			label.width = value;
			label.validate();
			label.y = int((value - label.height) * .5);
		}
		public function set data(value:BoxData):void
		{
			_data = value;
			_data.box = this;
			size = background.width;
			
			var savedProps:Object = _data.savedProps;
			if(savedProps)
			{
				isActive = savedProps.isActive;
				isPenciled = savedProps.isPenciled;
				view = isPenciled ? BoxView.PENCIL:BoxView.DEFAULT;
				label.visible = !isActive || isPenciled;
				text = isPenciled ? savedProps.penciledDigit:String(data.digit);
			}
		}
		public function set text(value:String):void
		{
			label.text = value;
			size = background.width;
		}
		public function get text():String
		{
			return label.text;
		}
		public function get data():BoxData
		{
			return _data;
		}
		public function digitCorrect(value:int):Boolean
		{
			return _data.digit == value;
		}
		override public function dispose():void
		{
			background = null;
			label = null;
			_view = null;
			if(_data)
			{
				_data.destroy();
				_data = null;
			}
			super.dispose();
		}
	}
}