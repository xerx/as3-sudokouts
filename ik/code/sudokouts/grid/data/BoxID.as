﻿package ik.code.sudokouts.grid.data 
{
	
	public class BoxID 
	{
		public var x:int;
		public var y:int;
		
		public function BoxID(x:int = 0, y:int = 0) 
		{
			this.x = x;
			this.y = y;
		}
		public function isEqual(ID:BoxID):Boolean
		{
			return x == ID.x && y == ID.y;
		}
		public function get key():String
		{
			return x + "_" + y;
		}
		public function toJSON():Object
		{
			return {x:this.x, y:this.y, key:this.key};
		}
		public function fromJSON(json:Object):void
		{
			x = json.x;
			y = json.y;
		}
	}
}
