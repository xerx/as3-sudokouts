﻿package ik.code.sudokouts.grid.data 
{
	import flash.utils.Dictionary;
	
	public class GridData 
	{
		public var dimension:int;
		private var boxesDict:Dictionary;
		private var boxesVec:Vector.<BoxData>;
		
		public function GridData(dimension:int) 
		{
			this.dimension = dimension;
			
			build();
		}
		private function build():void
		{
			boxesDict = new Dictionary();
			boxesVec = new <BoxData>[];
			
			var dimension2:int = dimension * dimension;
			var box:BoxData;
			for(var i:int = 0; i < dimension2; i++)
			{
				for(var j:int = 0; j < dimension2; j++)
				{
					box = new BoxData(i, j, this);
					boxesDict[box.ID.key] = box;
					boxesVec.push(box);
				}
			}
		}
		public function initializeBoxesDigits():void
		{
			var i:int = 0; 
			var initialized:Boolean;
			var numBoxesBeforeCurrent:int;
			
			while(i < boxesVec.length)
			{
				initialized = boxesVec[i].initializeDigit();
				if(!initialized)
				{
					if(i >= ++numBoxesBeforeCurrent)
					{
						initialized = boxesVec[i - numBoxesBeforeCurrent].initializeDigit();
						if(!initialized && i == numBoxesBeforeCurrent)
						{
							i = numBoxesBeforeCurrent = 0;
							resetBoard();
						}
					}
				}
				else
				{
					numBoxesBeforeCurrent = 0;
					i++;
				}
			}
		}
		public function initWithSavedBoxes(savedBoxesProps:Array):void
		{
			for (var i:int = 0; i < savedBoxesProps.length; i++) 
			{
				boxesDict[savedBoxesProps[i].id.key].fromJSON(savedBoxesProps[i]);
			}
		}
		private function resetBoard():void
		{
			for(var i:int = 0; i < boxesVec.length; i++)
				boxesVec[i].digit = 0;
		}
		internal function getBoxDigitWithKey(key:String):int
		{
			return boxesDict[key] == undefined ? -1:boxesDict[key].digit;
		}
		public function boxWithKeyIsInactive(key:String):Boolean
		{
			return !boxesDict[key].box.active;
		}
		public function getHintBoxID():BoxID
		{
			var maxPointsID:BoxID;
			var maxPoints:int = -1;
			var boxPoints:int;
			
			for(var i:int = 0; i < boxesVec.length; i++)
			{
				boxPoints = boxesVec[i].hintPoints;
				if(boxPoints > maxPoints)
				{
					maxPoints = boxPoints;
					maxPointsID = boxesVec[i].ID;
				}
			}
			return maxPointsID;
		}
		public function get boxesData():Dictionary
		{
			return boxesDict;
		}
		public function destroy():void
		{
			for(var i:int = 0; i < boxesVec.length; i++)
				boxesVec[i].destroy();
			boxesVec = null;
			boxesDict = null;
		}
		public function toJSON():Object
		{
			var json:Object = {boxes:[]};
			for (var i:int = 0; i < boxesVec.length; i++) 
			{
				json.boxes.push(boxesVec[i].toJSON());
			}
			return json;
		}
		public function toString():String
		{
			var lines:String = "";
			var dimension2:int = dimension * dimension;
			for(var i:int = 0; i < dimension2; i++)
			{
				for(var j:int = 0; j < dimension2; j++)
				{
					lines += boxesDict[j + "_" + i].getDigit() + "\t";
					if((j + 1) % dimension == 0)
						lines += "|\t";
				}
				if((i + 1) % dimension == 0)
					lines += "\n-----------------------------------------------------------------------------------------\n";
				else
					lines += "\n\n";
			}
			return lines;
		}
		
	}
	
}
