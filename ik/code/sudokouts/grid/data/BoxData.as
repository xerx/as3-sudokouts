﻿package  ik.code.sudokouts.grid.data
{
	import ik.code.sudokouts.grid.Box;
	
	
	public class BoxData 
	{		
		public var ID:BoxID;
		public var box:Box;
		public var digit:int;
		
		private var relativeBoxIDs:Vector.<BoxID>;
		private var legitDigits:Vector.<int>;
		
		private var gridData:GridData;
		private var savedObject:Object;
		
		public function BoxData(x:int, y:int, grid:GridData) 
		{
			this.gridData = grid;
			ID = new BoxID(x, y);
			
			initialize();
		}
		private function initialize():void
		{
			relativeBoxIDs = new <BoxID>[];
			legitDigits = new <int>[];
			
			for(var i:int = 0; i < gridData.dimension * gridData.dimension; i++)
			{
				addBoxID(ID.x, i);
				addBoxID(i, ID.y);
				legitDigits.push(i + 1);
			}
			
			var blockID:BoxID = new BoxID(Math.floor(ID.x / gridData.dimension), 
										  Math.floor(ID.y / gridData.dimension));
			
			var xStart:int = blockID.x * gridData.dimension;
			var yStart:int = blockID.y * gridData.dimension;
			for(i = xStart; i < xStart + gridData.dimension; i++)
			{
				for(var j:int = yStart; j < yStart + gridData.dimension; j++)
				{
					addBoxID(i, j);
				}
			}
		}
		private function addBoxID(x:int, y:int):void
		{
			var newID:BoxID = new BoxID(x, y);
			if(ID.isEqual(newID))
				return;

			for(var i:int = 0; i < relativeBoxIDs.length; i++)
			{
				if(newID.isEqual(relativeBoxIDs[i]))
					return;
			}
			relativeBoxIDs.push(newID);
		}
		public function initializeDigit():Boolean
		{
			var available:int = getFirstAvailableDigit();
			
			if(available == digit || available == 0)
				return false;
			
			digit = available;
			return true;
		}
		private function getFirstAvailableDigit():int
		{
			var tempDigits:Vector.<int> = legitDigits.concat();
			var boxDigit:int;
			var digitIndex:int;
			
			for(var i:int = 0; i < relativeBoxIDs.length; i++)
			{
				boxDigit = gridData.getBoxDigitWithKey(relativeBoxIDs[i].key);
				digitIndex = tempDigits.indexOf(boxDigit);
				if(digitIndex > -1)
				{
					tempDigits.splice(digitIndex, 1);
					if(tempDigits.length == 0)
						return 0;
				}
			}
			return tempDigits[Math.floor(Math.random() * tempDigits.length)];
		}
		public function get hintPoints():int
		{
			if(!box.active)
				return 0;
			
			var points:int;
			
			for(var i:int = 0; i < relativeBoxIDs.length; i++)
			{
				if(gridData.boxWithKeyIsInactive(relativeBoxIDs[i].key))
					points++;
			}
			return points;
		}
		public function toJSON():Object
		{
			return {
				id:ID.toJSON(),
				digit:this.digit,
				isActive:this.box.active,
				isPenciled:this.box.isPenciled,
				penciledDigit:this.box.isPenciled ? this.box.text:null
			};
		}
		public function fromJSON(json:Object):void
		{
			savedObject = json;
			ID = new BoxID();
			ID.fromJSON(savedObject.id);
			digit = savedObject.digit;
		}
		public function get savedProps():Object
		{
			if(savedObject)
				return {
					isActive:savedObject.isActive,
					isPenciled:savedObject.isPenciled,
					penciledDigit:savedObject.penciledDigit
				};
			return null;
		}
		public function destroy():void
		{
			ID = null;
			box = null;
			savedObject = null;
			relativeBoxIDs = null;
			legitDigits = null;
			gridData = null;
		}


	}
	
}
