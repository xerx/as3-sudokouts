package ik.code.sudokouts.grid
{
	import com.greensock.TweenLite;
	
	import flash.utils.Dictionary;
	
	import ik.code.sudokouts.game.Level;
	import ik.code.sudokouts.game.SaveObject;
	import ik.code.sudokouts.grid.data.BoxID;
	import ik.code.sudokouts.grid.data.GridData;
	import ik.code.sudokouts.keyboard.KeyboardButton;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	
	public class Grid extends Sprite
	{
		private static const GAP:int = 1;
		
		private var data:GridData;

		private var dimension:int;
		private var size:int;
		private var isOccupied:Boolean;

		private var background:Quad;
		private var level:Level;

		private var boxes:Dictionary;
		private var focusedBox:Box;
		
		public function Grid(size:int, dimension:int)
		{
			super();
			this.size = size;
			this.dimension = dimension;
			
			
			background = new Quad(size, size, 0);
			addChild(background);
		}
		
		public function build(savedObject:SaveObject):void
		{
			data = new GridData(dimension);
			
			if(savedObject)
			{
				data.initWithSavedBoxes(savedObject.boxes);
				level = Level.getLevelByName(savedObject.level);
			}
			else
			{
				data.initializeBoxesDigits();
			}
			
			var boxesData:Dictionary = data.boxesData;
			var dimension2:int = dimension * dimension;
			var gapsSum:int = ((dimension + 1) * (GAP + 2)) + (dimension * 2 * GAP);
			var boxSize:int = (size - gapsSum) / dimension2;
			
			background.width = background.height = (boxSize * dimension2) + gapsSum;
			
			var box:Box;
			var xPosition:int;
			var yPosition:int;
			
			boxes = new Dictionary();
			
			for(var i:int = 0; i < dimension2; i++)
			{
				xPosition = 0;
				yPosition += (i % dimension ? GAP:GAP + 2);
				for(var j:int = 0; j < dimension2; j++)
				{
					xPosition += (j % dimension ? GAP:GAP + 2);
					box = new Box();
					box.data = boxesData[j + "_" + i];
					box.size = boxSize;
					box.x = xPosition;
					box.y = yPosition;
					addChild(box);
					boxes[j + "_" + i] = box;
					xPosition += boxSize; 
				}
				yPosition += boxSize;
			}
			focusWithID(0, 0);
		}
		public function getLevel():Level
		{
			return level;
		}
		public function setLevel(value:Level):void
		{
			level = value;
			solveRandomBoxesByLevel();
			focusWithID(0, 0);
		}
		private function solveRandomBoxesByLevel():void
		{
			var randomI:int;
			var randomJ:int;
			var box:Box;
			var dimension2:int = dimension * dimension;
			var minPerSquare:int = Math.floor(level.givenBlocks / dimension2);
			var remainder:int = level.givenBlocks - (minPerSquare * dimension2);
			var iterationsLeft:int = dimension2; 
			var boxesPerSquare:int;
			
			for (var i:int = 0; i < dimension2; i+=dimension) 
			{
				for (var j:int = 0; j < dimension2; j+=dimension) 
				{
					boxesPerSquare = minPerSquare;
					if(remainder)
					{
						if(iterationsLeft > remainder)
						{
							if(Math.random() > .4)
							{
								remainder--;
								boxesPerSquare++;
							}
						}
						else
						{
							remainder--;
							boxesPerSquare++;
						}
					}
					iterationsLeft--;
					for (var k:int = 0; k < boxesPerSquare; k++)
					{
						do
						{
							randomI = i + Math.floor(Math.random() * dimension);
							randomJ = j + Math.floor(Math.random() * dimension);
							box = boxes[randomJ + "_" + randomI];
						}
						while(!box.active);
						
						box.view = BoxView.DEFAULT;
						box.active = false;
						box.labelVisible = true;
						box.text = String(box.data.digit);
					}
				}
			}
			
		}
		public function focusWithID(i:int, j:int, hint:Boolean = false):void
		{
			if(isOccupied)
				return;
			
			if(focusedBox)
			{
				focusedBox.view = focusedBox.isPenciled ? BoxView.PENCIL:BoxView.DEFAULT;
			}
			focusedBox = boxes[j + "_" + i];
			focusedBox.view = hint ? BoxView.HINT:BoxView.FOCUSED;
		}
		public function focusWithDirection(direction:String):void
		{
			if(!focusedBox || isOccupied)
				return;
			
			var focusedID:BoxID = focusedBox.data.ID;
			var dimension2:int = dimension * dimension;
			var j:int;
			var i:int;
			
			switch(direction)
			{
				case KeyboardButton.UP:
					i = focusedID.y > 0 ? focusedID.y - 1:dimension2 - 1;
					j  = focusedID.x;
					break;
				case KeyboardButton.DOWN:
					i = focusedID.y < dimension2 - 1 ? focusedID.y + 1:0;
					j  = focusedID.x;
					break;
				case KeyboardButton.RIGHT:
					i = focusedID.y
					j  = focusedID.x < dimension2 - 1 ? focusedID.x + 1:0;
					break;
				case KeyboardButton.LEFT:
					i = focusedID.y
					j  = focusedID.x > 0 ? focusedID.x - 1:dimension2 - 1;
					break;
			}
			focusWithID(i, j);
		}
		public function get focusedBoxIsActive():Boolean
		{
			return focusedBox.active;
		}
		public function focusOnHint():void
		{
			var id:BoxID = data.getHintBoxID();
			focusWithID(id.y, id.x, true);
		}
		public function userInput(keyID:String, pencil:Boolean):Boolean
		{
			var isCorrect:Boolean;
			
			if(pencil)
			{
				focusedBox.isPenciled = true;
				focusedBox.labelVisible = true;
				focusedBox.text = keyID;
			}
			else
			{
				if(focusedBox.digitCorrect(int(keyID)))
				{
					focusedBox.active = false;
					focusedBox.labelVisible = true;
					focusedBox.text = keyID;
					focusedBox.isPenciled = false;
					focusedBox.view = BoxView.FOCUSED;
					isCorrect = true;
				}
				else
				{
					focusedBox.view = BoxView.WRONG;
					focusedBox.labelVisible = true;
					focusedBox.text = keyID;
					isOccupied = true;
					
					TweenLite.delayedCall(.8, function():void{
						focusedBox.view = BoxView.FOCUSED;
						focusedBox.labelVisible = false;
						isOccupied = false;
					});
				}
			}
			return isCorrect;
		}
		public function get isComplete():Boolean
		{
			for each (var box:Box in boxes) 
			{
				if(box.active)
					return false;
			}
			return true;
		}
		public function boxesSaveData():Array
		{
			return data.toJSON().boxes;
		}
		override public function dispose():void
		{
			if(data)
			{
				data.destroy();
				data = null;
			}
			if(focusedBox)
			{
				focusedBox.dispose();
				focusedBox = null;
			}
			background = null;
			boxes = null;
			super.dispose();
		}
	}
}