package ik.code.sudokouts.grid
{
	public class BoxView
	{
		public static const DEFAULT:BoxView = new BoxView("DEFAULT", 0xFFFFFF, 0x333333);
		public static const FOCUSED:BoxView = new BoxView("FOCUSED", 0x79BACA, 0xFFFFFF);
		public static const PENCIL:BoxView = new BoxView("PENCIL", 0xBBBBBB, 0xFFFFFF);
		public static const WRONG:BoxView = new BoxView("WRONG",0xC03221, 0xFFFFFF);
		public static const HINT:BoxView = new BoxView("HINT", 0xFF7353, 0xFFFFFF);
		
		public var color:uint;
		public var fontColor:uint;
		private var name:String;
		
		public function BoxView(name:String, color:uint, fontColor:uint)
		{
			this.name = name;
			this.fontColor = fontColor;
			this.color = color;
		}
		public function toString():String
		{
			return name;
		}
		public static function fromName(name:String):BoxView
		{
			return BoxView[name];
		}
	}
}